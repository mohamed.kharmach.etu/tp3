import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

//B.2.1
//1
const listPizzas = document.querySelectorAll('section > article a h4');
console.log(listPizzas[1].innerHTML);

//2
const logo = document.querySelector('.logo');
logo.innerHTML += "<small>les pizzas c'est la vie</small>";

//B.2.2
//1
const listLien = document.querySelectorAll('footer > div a');
console.log(listLien[1].getAttribute('href'));

//2
const laCarte = document.querySelector('.pizzaListLink');
//laCarte.setAttribute('class', laCarte.getAttribute('class') + ' active');
laCarte.classList.add('active'); //Une autre façon de faire l31
